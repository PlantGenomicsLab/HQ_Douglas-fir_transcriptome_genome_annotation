##  A long-read and short-read transcriptomics approach provides the first high-quality reference transcriptome and genome annotation for Pseudotsuga menziesii (Douglas-fir)  
  
Authors: Vera Marjorie Elauria Velasco, Alyssa Ferreira, Sumaira Zaman, Devin Noordermeer, Ingo Ensminger and Jill L. Wegrzyn  

JOURNAL ARTICLE available at [G3](https://doi.org/10.1093/g3journal/jkac304)
  
Here, we show the steps taken to significantly improve the record of transcripts and genes in Douglas-fir.  We used long-reads (LR) generated from PacBio Iso-Seq and paired-end 100bp short-reads (SR) generated using Illumina NovaSeq.    

### Original resources are available at [NCBI BioProject ID PRJNA614528](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA614528)  
- BioSamples Accession nos. SAMN15501818 to SAMN15501821    
  BioSamples are from a total of four individual Douglas-fir plants exposed to control and stressed treatments.  
- Iso-Seq subreads Accession nos. SRR12208323 to SRR12208326  
- NovaSeq RNASeq raw reads (paired-end SR) Aaccession nos.  SRR12208319 to SRR12208322  
- Iso-Seq _de novo_ assembled transcriptome NCBI TSA Accession No. GISH00000000   
- Iso-Seq reference-genome guided transcriptome assembly NCBI TSA Accession No. GISF00000000   
- ### Genome annotation files are available at [TreeGenesDB](https://treegenesdb.org/FTP/Genomes/Psme/v1.0) 


### Published datasets used  
  
- single-end SR transcriptome at GenBank Short Read Archive SRP018395 GEO Accession GSE44058      
    Transcription through the eye of a needle: daily and annual cyclic gene expression variation in Douglas-fir needles: Richard Cronn, Peter C. Dolan, Sanjuro Jogdeo, David B. Neale, J. Bradley St. Clair, Dee R. Denver, Jill Wegrzyn, 2017

- Douglas-fir genome at  NCBI as accession [LPNX000000000](https://www.ncbi.nlm.nih.gov/nuccore/LPNX000000000) in BioProject PRJNA174450   
    The Douglas-Fir Genome Sequence Reveals Specialization of the Photosynthetic Apparatus in Pinaceae: David B. Neale, Patrick E. McGuire, Nicholas C. Wheeler, Kristian A. Stevens, Marc W. Crepeau et al., Jill Wegrzyn, 2017  

### Workflow summary  

![alt text](WORKFLOW.png "")

A.  Iso-Seq LR data quality control and transcriptome assembly    
1.  [Quality control using isoseq3](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Isoseq3-quality-control)    
1.  [LR _de novo_ transcriptome assembly](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Reference-transcriptome)   
1.  [LR reference-guided transcriptome assembly](https://gitlab.com/douglas-fir-transcriptome/Genome-guided-assembly-of-long-reads)    
1.  Assembly quality assesnment with BUSCO and rnaQUAST
1.  Functional annotation with enTAP

B.  [Identification of Transcription Factor (TF) from Iso-Seq LR data using TFPredict](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Identify-TF)

C.  [Identification of long non-coding RNA from Iso-Seq LR data using CREMA](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Predict-lncRNA) 

D.  [Illumina SR quality control and transcriptome assembly](https://gitlab.com/douglas-fir-transcriptome/De-novo-assembly-of-short-reads) 
1. Pre-processing and quality control using Trimmomatic, FastQC and multiQC
1. SR _de novo_ assembly using Trinity
1. Assembly quality assessment using BUSCO and rnaQUAST

E.  Genome annotation  
1. ["transcriptome alignment"](https://gitlab.com/PlantGenomicsLab/genome-annotation-of-douglas-fir/-/tree/master/0_Transcriptome_Alignment)  Combined LR, paired-end and single-end SR transcriptome alignment to genome   
1. ["Annotation v1"](https://gitlab.com/PlantGenomicsLab/genome-annotation-of-douglas-fir/-/tree/master/1_Original_Maker_Annotation)  Annotation using combined transcriptomes and MAKER-P 
1. ["Annotation v2 (pre-filter)"](https://gitlab.com/PlantGenomicsLab/genome-annotation-of-douglas-fir/-/tree/master/2_Braker_Annotation)  Annotation using combined transcriptomes, NCBI and [custom](https://gitlab.com/PlantGenomicsLab/genome-annotation-of-douglas-fir/-/blob/master/conifer_geneSet_protein_v2_150.faa) genesets, and BRAKER2
1. ["Annotation v2"](https://gitlab.com/PlantGenomicsLab/genome-annotation-of-douglas-fir/-/tree/master/3_Braker_Annotation_Filtered) Filtering annotation generated using combined transcriptomes and BRAKER2
1. Genome annotation comparisons using gFACs statistics, EnTAP, and BUSCO

F.  Work performed by Cronn et al. 2017  




